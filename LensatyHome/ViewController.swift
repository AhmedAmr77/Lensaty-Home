//
//  ViewController.swift
//  LensatyHome
//
//  Created by Ahmed Amr on 8/2/20.
//  Copyright © 2020 Ahmed Amr. All rights reserved.
//

import UIKit
import FSPagerView

class ViewController: UIViewController {
    
    @IBOutlet weak var collVi: UICollectionView!
    @IBOutlet weak var pgVi: FSPagerView! {
        didSet {
            self.pgVi.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "celly")
        }
    }
    
    let pgViArr = ["DAHAB", "ANESTH", "INTER", "SILVER"]
    let arr = [["DAHAB", "DAHAB"],
                ["ANESTH", "ANESTHESIA"],
                ["INTER", "INTER"],
                ["SILVER", "SILVER"],
                ["ANESTH", "BRANDY"],
                ["INTER", "LENSY"],
                ["SILVER", "COLORY"]]

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collVi!.register(myCell.nib(), forCellWithReuseIdentifier: "cell")
       
        configCollVi()
        configPgVi()
        barButton()
        
        navigationController?.navigationBar.barTintColor = UIColor.systemYellow

    }
   
    @objc func searchTapped() {

        print("SEARCH PRESSED")
    }
    
    func barButton() {
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "menuIcon"), for: .normal)
        button.addTarget(self, action: #selector(menuButtonPressed), for: .touchUpInside)
        //button.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        
        let menuButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = menuButton
        
        let searchButton = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(searchTapped))
        searchButton.tintColor = .black
        self.navigationItem.rightBarButtonItem = searchButton
    }
    @objc func menuButtonPressed() {

        print("MENU PRESSED")
    }
    
    func configCollVi() {
        let itemSize = UIScreen.main.bounds.width / 2 - 20
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: itemSize, height: itemSize)
        layout.minimumLineSpacing = -5
        layout.minimumInteritemSpacing = 2
        collVi.collectionViewLayout = layout
    }
    
    func configPgVi() {
        // Create a pager view
        let pagerView = FSPagerView()
        pagerView.dataSource = self
        pagerView.delegate = self
        pagerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "celly")
        self.view.addSubview(pagerView)
        // Create a page control
        let pageControl = FSPageControl()
        self.view.addSubview(pageControl)
        
        pgVi.automaticSlidingInterval = 3.0
        pgVi.isInfinite = true
//        pageControl.numberOfPages = 4
//        pageControl.contentHorizontalAlignment = .right
//        pageControl.setStrokeColor(.black, for: .normal)
//        pageControl.setStrokeColor(.white, for: .selected)
//        pageControl.setFillColor(.lightGray, for: .normal)
//        pageControl.setFillColor(.white, for: .selected)
//
//        pageControl.setPath(UIBezierPath(rect: CGRect(x: 360, y: 270, width: 8, height: 8)), for: .normal)
//        pageControl.setPath(UIBezierPath(ovalIn: CGRect(x: 360, y: 270, width: 8, height: 8)), for: .selected)
    }

}

extension ViewController:  UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arr.count
    }
       
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! myCell
        cell.btn.setImage(UIImage(named: arr[indexPath.row][0]), for: .normal)
        cell.lbl.text = arr[indexPath.row][1]
        
        return cell
    }
}

extension ViewController: FSPagerViewDataSource, FSPagerViewDelegate {
    func numberOfItems(in pagerView: FSPagerView) -> Int {
            return pgViArr.count
        }
        
        func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
            let cell = pgVi.dequeueReusableCell(withReuseIdentifier: "celly", at: index)
            cell.imageView?.image = UIImage(named: pgViArr[index])
            return cell
        }
}
