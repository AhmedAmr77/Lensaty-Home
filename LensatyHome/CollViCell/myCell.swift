//
//  myCell.swift
//  LensatyHome
//
//  Created by Ahmed Amr on 8/2/20.
//  Copyright © 2020 Ahmed Amr. All rights reserved.
//

import UIKit

class myCell: UICollectionViewCell {

    @IBOutlet weak var lbl: UILabel!
    @IBOutlet weak var btn: UIButton!
    
    static func nib() -> UINib {
        return UINib(nibName: "myCell", bundle: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    @IBAction func btnPressed(_ sender: UIButton) {
        print("Button PRESSED")
    }
    
}
